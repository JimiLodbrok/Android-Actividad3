package com.example.jaime.actividad3.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.example.jaime.actividad3.R;
import com.example.jaime.actividad3.model.firebase.FirebaseAdmin;
import com.example.jaime.actividad3.model.firebase.FirebaseLoginListener;
import com.example.mylibrary.events.EventsAdmin;
import com.example.mylibrary.events.EventsListener;
import com.example.mylibrary.fragments.LoginFragment;
import com.example.mylibrary.fragments.RegisterFragment;

public class ActivityLogin extends AppCompatActivity implements EventsListener, FirebaseLoginListener {
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.loginFragment);
        registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentById(R.id.registerFragment);

        FirebaseAdmin.getInstance().setLoginListener(this);
        FirebaseAdmin.getInstance().setActivity(this);
        EventsAdmin.getInstance().addListener(this);

        onReturnClicker();
    }

    public void onLoginClicked() {
        if (!loginFragment.getEdtxt_User().getText().toString().equals("") && !loginFragment.getEdtxt_Password().getText().toString().equals(""))
            FirebaseAdmin.getInstance().signIn(loginFragment.getEdtxt_User().getText().toString(), loginFragment.getEdtxt_Password().getText().toString());
        else
            Toast.makeText(this, "Fill the sign in fields",
                    Toast.LENGTH_SHORT).show();

    }

    public void onNewRegisterClicker() {
        if (!registerFragment.getEdtxt_NewUser().getText().toString().equals("") && !registerFragment.getEdtxt_NewPassword().getText().toString().equals(""))
            if (registerFragment.getEdtxt_NewPassword().getText().toString().equals(registerFragment.getEdtxt_NewPassword2().getText().toString())) {
                loginFragment.getEdtxt_User().setText(registerFragment.getEdtxt_NewUser().getText().toString());
                FirebaseAdmin.getInstance().createAccount(registerFragment.getEdtxt_NewUser().getText().toString(), registerFragment.getEdtxt_NewPassword().getText().toString());
            } else
                Toast.makeText(this, "The passwords have to match",
                        Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Pleas fill all the fields.",
                    Toast.LENGTH_SHORT).show();
    }

    public void onRegisterClicked() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(loginFragment);
        transition.show(registerFragment);
        transition.commit();
        clearLoginFragment();
    }

    public void onReturnClicker() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(registerFragment);
        transition.show(loginFragment);
        transition.commit();
        clearRegisterFragment();
    }

    public void clearLoginFragment() {
        loginFragment.getEdtxt_User().setText("");
        loginFragment.getEdtxt_Password().setText("");
    }

    public void clearRegisterFragment() {
        registerFragment.getEdtxt_NewUser().setText("");
        registerFragment.getEdtxt_NewPassword().setText("");
        registerFragment.getEdtxt_NewPassword2().setText("");
    }

    @Override
    public void onClickScren(View view) {
        if (view.getId() == R.id.btnLogin) {
            onLoginClicked();
        } else if (view.getId() == R.id.btnRegister)
            onRegisterClicked();
        else if (view.getId() == R.id.btnNRegister) {
            onNewRegisterClicker();
        } else if (view.getId() == R.id.btnVolver)
            onReturnClicker();
    }

    @Override
    public void userSignedIn() {
        Intent secondActivity = new Intent(this, ActivityList.class);
        startActivity(secondActivity);
    }

    @Override
    public void userCreated() {
        onReturnClicker();
        clearRegisterFragment();
    }

}
