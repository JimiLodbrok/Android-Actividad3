package com.example.jaime.actividad3.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jaime.actividad3.R;
import com.example.jaime.actividad3.model.firebase.FirebaseAdmin;
import com.example.jaime.actividad3.model.firebase.FirebaseDataListener;
import com.example.jaime.actividad3.model.instance.DataHolder;
import com.example.jaime.actividad3.model.persistence.Jugador;
import com.example.jaime.actividad3.model.persistence.Perfil;
import com.example.jaime.actividad3.utils.adapters.ListAdapter;
import com.example.mylibrary.GPSAdmin.GPSTracker;
import com.example.mylibrary.fragments.DetalleMapFragment;
import com.example.mylibrary.fragments.ListFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static android.content.ContentValues.TAG;

public class ActivityList extends AppCompatActivity implements FirebaseDataListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private TextView display;
    //private ListFragment listFragment;
    private SupportMapFragment mapFragment;
    private DetalleMapFragment detMapFragment;
    private GoogleMap mMap;
    private Perfil perfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        display = findViewById(R.id.textDisplay);
        display.setText("Hello " + FirebaseAdmin.getInstance().getCurrentUser().getEmail().toString() + "!!!");

        Log.v(TAG,"E-MAIL: " + FirebaseAdmin.getInstance().getCurrentUser().getEmail().toString());
        Log.v(TAG,"UID: " + FirebaseAdmin.getInstance().getCurrentUser().getUid().toString());

        //listFragment = (ListFragment) getSupportFragmentManager().findFragmentById(R.id.listFragment);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        detMapFragment = (DetalleMapFragment)  getSupportFragmentManager().findFragmentById(R.id.detalleMapFragment);
        mapFragment.getMapAsync(this);

        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(detMapFragment);
        transition.commit();

        FirebaseAdmin.getInstance().setDataListener(this);

        inicializaGPSTracker();

        crearFragmentLista();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataLoaded();
        crearFragmentLista();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
    }

    public void crearFragmentLista() {
        FirebaseAdmin.getInstance().loadData("jugadores");
        //FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        //transition.show(listFragment);
        //transition.commit();
    }

    public void inicializaGPSTracker() {
        GPSTracker tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {
            Log.v("SecondActivity",tracker.getLatitude() + "   " + tracker.getLongitude());
            perfil = new Perfil("Jimigar",FirebaseAdmin.getInstance().getCurrentUser().getUid(), tracker.getLatitude(), tracker.getLongitude());
            FirebaseAdmin.getInstance().insertData("/perfiles/" + FirebaseAdmin.getInstance().getCurrentUser().getUid(), perfil.toMap());
        } else {
            tracker.showSettingsAlert();
        }
    }

    @Override
    public void dataLoaded() {
        //listFragment.getRecyclerView().setAdapter(new ListAdapter(this));
    }

    @Override
    public void addMarkers() {
        for (int i = 0; i < DataHolder.getInstance().getJugadores().size(); i++) {
            LatLng position = new LatLng(DataHolder.getInstance().getJugadores().get(i).getLat(), DataHolder.getInstance().getJugadores().get(i).getLon());
            MarkerOptions markOp = new MarkerOptions();
            markOp.position(position);
            markOp.title(DataHolder.getInstance().getJugadores().get(i).getNombre());
            if (mMap != null) {
                DataHolder.getInstance().getJugadores().get(i).setMarker(mMap.addMarker(markOp));
                DataHolder.getInstance().getJugadores().get(i).getMarker().setTag(DataHolder.getInstance().getJugadores().get(i));
                if (i == 0) mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position,4));
            }
            /*
            LatLng positionPerfil = new LatLng(perfil.getLat(),perfil.getLon());
            MarkerOptions markOpPerfil = new MarkerOptions();
            markOpPerfil.position(positionPerfil);
            markOpPerfil.title(perfil.getNombre());
            if (mMap != null) {
                perfil.setMarker(mMap.addMarker(markOpPerfil));
                perfil.getMarker().setTag(perfil);
                if (i == 0) mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionPerfil,10));
            }*/
        }
    }

    @Override
    public void removeMarkers() {
        for (int i = 0; i < DataHolder.getInstance().getJugadores().size(); i++) {
            if(DataHolder.getInstance().getJugadores().get(i).getMarker() != null) {
                DataHolder.getInstance().getJugadores().get(i).getMarker().remove();
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Jugador jugador = (Jugador) marker.getTag();
        Log.v("SecondActivity", "PRESIONADO EN PIIIIIN: " + jugador.getNombre());
        detMapFragment.getNombreJugador().setText(jugador.getNombre());
        detMapFragment.getAlturaJugador().setText(String.valueOf(jugador.getAltura()));
        detMapFragment.getEquipoJugador().setText(jugador.getEquipo());
        if(jugador.getUrlImage() != null)
            Glide.with(this).load(jugador.getUrlImage()).into(detMapFragment.getImagenJugador());
        else
            Glide.with(this).load("https://cuadrosyvinilos.es/content/upload/master/thumb/470/59361fb8-0d4e-4ed5-a53a-6d0f91a16750.png").into(detMapFragment.getImagenJugador());
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.show(detMapFragment);
        transition.commit();
        return false;
    }
}
